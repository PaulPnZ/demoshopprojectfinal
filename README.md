# Final project for Demo shop application testing

## Short Description
This application is responsible for testing the main functionalities of the Demo Shop app.
 - Checkout
 - Product Listing
 - Login
 - Add To wishlist


# Demo Shop Test Automation Framework
A brief presentation of my project

## This is the final project for Alexandrescu Paul, within the FastTrackIt Test Automation course.

### Software engineer: _Alexandrescu Paul_

### Tech stack used:
    - Java17
    - Maven
    - Selenide Framwork
    - PageObject Models

### How to run the tests
`git clone https://gitlab.com/PaulPnZ/demoshopprojectfinal.git`

Execute the following commands to:
#### Execute all tests
- `mvn clean test`
#### Generate Report
- `mvn allure:report`
> Open and present report
- `mvn allure:serve`

#### Page Objects
    - HomePage
    - Header
    - Footer
    - Body / Products

#### Number Of Tests : 95

- Reporting is also available in the Allure-results folder
    - tests are executed using maven


### Tests defintion
| TestID | Test Name | Test Description | Test Status|

