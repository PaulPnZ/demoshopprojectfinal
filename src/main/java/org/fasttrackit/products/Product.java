package org.fasttrackit.products;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Product {
    private final String productId;
    private final SelenideElement productLink;
    private final SelenideElement card;
    private final SelenideElement addToCartButton;
    private final SelenideElement addToWishlistButton;
    private final SelenideElement removeFromWishlistButton;
    private final ElementsCollection counterProducts = $$(".fa-layers-counter");
    private final ProductExpectedResults expectedResults;

    public Product(String productId, ProductExpectedResults expectedResults) {
        this.productId = productId;
        this.productLink = $(String.format(".card-body [href='#/product/%s']", productId));
        this.expectedResults = expectedResults;
        this.card = productLink.parent().parent();
        this.addToCartButton = card.$(".fa-cart-plus");
        this.addToWishlistButton = card.$(".fa-heart");
        this.removeFromWishlistButton = card.$(".fa-heart-broken");
    }

    public String getProductId() {
        return productId;
    }

    public void clickOnProduct() {
        this.productLink.scrollTo();
        this.productLink.click();
    }

    public void addProductToCart() {
        this.addToCartButton.scrollTo();
        this.addToCartButton.click();
    }

    public void addProductToWishlist() {
        this.addToWishlistButton.scrollTo();
        this.addToWishlistButton.click();
    }

    public void removeProductFromWishlist () {
        this.removeFromWishlistButton.click();
    }

    public ProductExpectedResults getExpectedResults() {
        return expectedResults;
    }

    @Override
    public String toString() {
        return this.productLink.text();
    }
}
