package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class MainPage extends Page {
    private final String title = Selenide.title();

    public SelenideElement getSearchButton() {
        return searchButton;
    }

    private final SelenideElement searchButton = $(".btn-light");
    private final SelenideElement searchField = $("#input-search");
    private final SelenideElement sortField = $(".sort-products-select");
    private Header header;
    private final Footer footer;
    private final SelenideElement modal = $(".modal-dialog");

    public MainPage() {
        System.out.println("Constructing Header");
        this.header = new Header();
        System.out.println("Constructing Footer");
        this.footer = new Footer();
    }

    public void returnToHomePage() {
        this.header.getLogoIconUrl().click();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return header;
    }

    public Footer getFooter() {
        return footer;
    }

    public String getPageTitle() {
        return title;
    }

    public String verifyThatTitleIsDisplayedOnScreen() {
        return title;
    }

    public void validateThatFooterContainsAllElements() {
        System.out.println("1. Verify footer details are: " + footer.getDetails());
        System.out.println("2. Verify footer has Question Icon: " + footer.getQuestionIcon());
        System.out.println("3. Verify footer reset Icon title: " + footer.getResetIconTitle());
    }

    public void validateThatHeaderContainsAllElements() {
        System.out.println("1. Verify that logo url is: " + header.getLogoIconUrl());
        System.out.println("2. Verify that shopping cart url is: " + header.getShoppingCartIconUrl());
        System.out.println("3. Verify that wishlist url is: " + header.getWishlistIconUrl());
        System.out.println("4. Verify that welcome message is: " + header.getGreetingsMessage());
        System.out.println("5. Verify that sign-in is: " + header.getSignInButton());
    }

    public boolean validateSortFieldIsDisplayed() {
        return sortField.exists() && sortField.isDisplayed();
    }

    public boolean validateThatSearchButtonIsDisplayed() {
        return this.searchButton.exists() && this.searchButton.isDisplayed();
    }

    public boolean validateModalIsDisplayed() {
        System.out.println("Verify that modal is displayed on page.");
        return this.modal.exists() && this.modal.isDisplayed();
    }

    public boolean validateModalIsNotDisplayed() {
        System.out.println("1. Verify that modal is not on page.");
        return !modal.exists();
    }

    public boolean validateThatSearchButtonIsEnabled () {
        return this.searchButton.isDisplayed() && this.searchButton.isEnabled();
    }
    public void clickOnTheSearchButton () {
        this.searchButton.click();
        sleep(100);
    }
    public void clickOnTheSearchField () {
        this.searchField.click();
        sleep(100);
    }
    public void typeInSearchField (String productToType) {
        this.searchField.sendKeys(productToType);
    }


    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();
        sleep(100);
    }

    public void clickOnTheWishlistButton() {
        this.header.clickOnTheWishlistButton();
    }

    public void clickOnTheLogoButton() {
        this.header.clickOnTheLogoButton();
        sleep(100);
    }

    public void logUserOut() {
        this.header.clickOnTheLogoutButton();
        sleep(150);
    }

    public void clickOnTheShoppingCartIcon (){
        this.header.clickOnShoppingCartIcon();
        sleep(150);
    }

    public void clickOnTheResetIcon() {
        this.getFooter().getResetIconTitle().click();
    }
}
