package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".btn-primary");
    private final SelenideElement errorMessage = $(".error");

    public Modal() {

    }

    public String getModalTitle() {
        return modalTitle.text();
    }

    public void validateModalComponents() {
        System.out.println("1. Verify that the password field is displayed.");
        System.out.println("2. Verify that the Login button is displayed.");
        System.out.println("3. Verify that the Login button is enabled.");
    }

    /**
     * Clicks
     */

    public void clickOnCloseButton() {
        System.out.println("Clicked on the 'x' button");
        this.closeButton.click();
        sleep(300);
    }

    public void clickOnUsernameField() {
        this.username.click();
    }

    public void clickOnPasswordField() {
        System.out.println("Clicked on the " + this.password);
    }

    public void clickOnSubmitButton() {
        System.out.println("Clicked on the Submit button");
        this.loginButton.click();
    }

    public void clickOnTheLoginButton() {
        this.loginButton.click();
        sleep(300);
    }

    /**
     * Type actions
     */

    public void typeInUsernameField(String userToType) {
        System.out.println("Typed in username: " + userToType);
        this.username.click();
        this.username.sendKeys(userToType);
    }

    public void typeInPasswordField(String passwordToType) {
        System.out.println("Typed in password: " + passwordToType);
        this.password.click();
        this.password.sendKeys(passwordToType);
    }

    /**
     * Validators
     */

    public boolean validateCloseButtonIsDisplayed() {
        return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean validateUsernameFieldIsDisplayed() {
        return this.username.exists() && this.username.isDisplayed();
    }

    public boolean validatePasswordFieldIsDisplayed() {
        return this.password.exists() && this.password.isDisplayed();
    }

    public boolean validateLoginButtonIsDisplayedAndIsEnabled() {
        return this.loginButton.isDisplayed() && this.loginButton.isEnabled();
    }

    public String getErrorMessage() {
        return errorMessage.text();
    }
}
