package org.fasttrackit;

public class Account {
    public final String username;
    public final String password;
    public final String emailAddress;

    public final String greetingsMsg;

    public Account(String username, String password, String emailAddress) {
        this.username = username;
        this.password = password;
        this.emailAddress = emailAddress;
        this.greetingsMsg = "Hi " + this.username + "!";
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getGreetingsMsg() {
        return greetingsMsg;
    }

    @Override
    public String toString() {
        return this.username;
    }
}
