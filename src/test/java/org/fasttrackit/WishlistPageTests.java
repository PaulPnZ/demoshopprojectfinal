package org.fasttrackit;

import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishlistPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WishlistPageTests {
    MainPage homepage = new MainPage();
    WishlistPage wishlistPage = new WishlistPage();

    @BeforeMethod
    public void setup(){
        homepage.clickOnTheResetIcon();
    }

    @AfterMethod
    public void returnToHomePage() {
        homepage.clickOnTheLogoButton();
    }


    @Test
    public void verify_the_wishlist_page_tittle_is_displayed() {
        homepage.clickOnTheWishlistButton();
        assertEquals(wishlistPage.getPageSubtitle(), "Wishlist", "Expected Wishlist page title to be: Wishlist");
        sleep(4000);
    }
}
