package org.fasttrackit;


import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.cart.ProductsInCartPage;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ProductsInCartPageTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @BeforeMethod
    public void reset () {
        homePage.clickOnTheResetIcon();
    }
    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
    }
    @Test(testName = "Cart Page elements test.",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void verify_cart_page_elements_test(Product product) {
        product.addProductToCart();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertTrue(cartPage.getPlusButton(), "Expected Increment Quantity Button to be displayed");
        assertTrue(cartPage.getMinusButton(),"Expected Decrement Quantity Button to be displayed");
        assertTrue(cartPage.validateProductCartTitleIsDisplayed(),"Expected Product Title to be displayed");
        assertTrue(cartPage.getTrashButton(), "Expected Trash Button to be displayed");
        assertTrue(cartPage.getContinueShoppingButton(), "Expected Continue Shopping Button to be displayed");
        assertTrue(cartPage.getCheckoutButton(), "Expected Checkout Button to be displayed");
    }

    @Test(testName = "User can add any product to cart test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void user_can_add_products_to_cart_test(Product product) {
        product.addProductToCart();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        sleep(1000);
        String expectedProductCartTitle = product.getExpectedResults().getTitle();
        assertEquals(cartPage.getProductCartTitle(), expectedProductCartTitle, "Expected product title added to cart to be: " + expectedProductCartTitle);
        sleep(1000);
    }

    @Test(testName = "User can remove products from cart test.",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void user_can_remove_products_from_cart_test(Product product) {
        product.addProductToCart();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        sleep(1000);
        cartPage.removeProductFromCart();
        assertEquals(cartPage.getCartPageMessage(),"How about adding some products in your cart?");
        sleep(1000);
    }

    @Test(testName = "Add the same product to cart multiple times test")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_add_the_same_product_to_cart_several_times_test() {
        Product p1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToCart();
        p1.addProductToCart();
        p1.addProductToCart();
        p1.addProductToCart();
        p1.addProductToCart();
        assertTrue(homePage.getHeader().validateShoppingBadgeIconIsDisplayed(), "Expected Shopping Badge icon to be displayed");
        assertEquals(homePage.getHeader().getShoppingBadgeIcon(), "5", "Expected Shopping Badge number to be: 5");
        sleep(3000);
    }

    @Test(testName = "Remove product from wishlist test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void user_can_remove_product_from_wishlist(Product p) {
        p.addProductToWishlist();
        p.removeProductFromWishlist();
        assertTrue(!homePage.getHeader().validateWishlistBadgeIconIsDisplayed(), "Expected wishlist badge icon to disappear");
    }
}

