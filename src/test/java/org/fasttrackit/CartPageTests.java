package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.Page;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class CartPageTests extends TestConfiguration {
    Page page;
    CartPage cartPage;
    @BeforeMethod
    public void setup(){
        page = new Page();
        cartPage = new CartPage();
    }
    @AfterMethod
    public void returnToHomePage() {
        cartPage.clickOnTheLogoButton();
    }

    @Test
    public void verifyCartPageTitleIsDisplayed() {
        assertEquals(cartPage.getCartPageTitle(), "Your cart", "Expected cart page title to be: Your cart");
        sleep(1000);
    }
    @Test
    public void verifyCartPageMessageIsDisplayed() {
        assertEquals(cartPage.getCartPageMessage(),"How about adding some products in your cart?");
        sleep(1000);
    }
}

