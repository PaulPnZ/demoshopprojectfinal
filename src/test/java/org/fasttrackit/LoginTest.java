package org.fasttrackit;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


@Feature("User Login")
public class LoginTest extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @Test(dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class,
            testName = "Login without password test",
            description = "Testing that user can't login without password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_page_without_password_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Please fill in the password!", "Expected error message to be: Please fill in the password!");
        modal.clickOnCloseButton();
        sleep(1 * 1000);
    }


    @Test(testName = "Modal components test",
            description = "Testing modal components are displayed and modal can be closed")
    @Severity(SeverityLevel.NORMAL)
    public void modal_components_are_displayed_and_modal_can_be_closed_test() {
        homePage.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal is displayed.");
        Modal modal = new Modal();
        assertEquals(modal.getModalTitle(), "Login", "Expected Modal title to be Login.");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected Close Button to be displayed.");
        assertTrue(modal.validateUsernameFieldIsDisplayed(), "Expected username field to be displayed.");
        modal.clickOnCloseButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed.");
        sleep(1 * 1000);
    }

    @Test(testName = "Login Test with valid User:",
            dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_login_on_the_demo_app_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed.");
        Header header = new Header(account.getUsername());
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Expected greetings message to contain the account user.");
        homePage.logUserOut();
        sleep(1 * 1000);
    }

    @Test
    public void user_cant_login_with_locked_account_test() {
        Account lockedAccount = new Account("locked", "choochoo", "emailAddress");
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(lockedAccount.getUsername());
        modal.typeInPasswordField(lockedAccount.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain opened.");
        assertEquals(modal.getErrorMessage(), "The user has been locked out.", "Expected error message to be: The user has been locked out.");
        modal.clickOnCloseButton();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_the_page_without_password_test() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.typeInUsernameField(user);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on the page.");
        modal.clickOnCloseButton();
    }

    @Test
    public void validate_message_mandatory_username_field_test() {
        homePage.clickOnTheLoginButton();
        Header header = new Header();
        Modal modal = new Modal();
        modal.clickOnSubmitButton();
        assertTrue(header.validateUsernameErrorMessageIsDisplayed(), " 'Please fill in the username!' message is displayed.");
        modal.clickOnCloseButton();
    }

    @Test(dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class,
            testName = "Login without username test",
            description = "Testing that user can't login without username")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_the_page_without_username_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Please fill in the username!", "Expected error message to be: Please fill in the username!");
        modal.clickOnCloseButton();
        sleep(1 * 1000);
    }

    @Test(dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class,
            testName = "Login with invalid username test",
            description = "Testing that user can't login with wrong username")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_the_page_with_wrong_username_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername() + "???");
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Incorrect username or password!", "Expected error message to be: Incorrect username or password!");
        modal.clickOnCloseButton();
        sleep(1 * 1000);
    }

    @Test(dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class,
            testName = "Login with invalid password test",
            description = "Testing that user can't login with wrong password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_the_page_with_wrong_password_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername() );
        modal.typeInPasswordField(account.getPassword() + "!!!");
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Incorrect username or password!", "Expected error message to be: Incorrect username or password!");
        modal.clickOnCloseButton();
        sleep(1 * 1000);
    }
}