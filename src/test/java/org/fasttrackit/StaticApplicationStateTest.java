package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static com.codeborne.selenide.Selenide.sleep;

public class StaticApplicationStateTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @Test
    public void verify_demo_shop_app_title() {
        String appTitle = homePage.getPageTitle();
        assertEquals(appTitle,"Demo shop","Application title is expected to be Demo shop.");
        sleep(5000);
    }

    @Test
    public void verify_demo_shop_footer_build_date_details() {
        String footerDetails = homePage.getFooter().getDetails();
        assertEquals(footerDetails,"Demo Shop | build date 2021-05-21 14:04:30 GTBDT","Expected footer details to be: Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
        sleep(5000);
    }

    @Test
    public void verify_demo_shop_footer_contains_question_icon() {
        SelenideElement questionIcon = homePage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(),"Expected Question Icon to exist on Page.");
        assertTrue(questionIcon.isDisplayed(),"Expected Question icon to be displayed.");
        sleep(5000);
    }

    @Test
    public void verify_demo_shop_footer_contains_reset_icon() {
        SelenideElement resetIconTitle = homePage.getFooter().getResetIconTitle();
        assertTrue(resetIconTitle.exists(),"Expected Reset Icon to exist on Page.");
        assertTrue(resetIconTitle.isDisplayed(),"Expected Reset Icon to be displayed.");
        sleep(5000);
    }

//    @Test
//    public void verify_sort_field_is_displayed(){
//        assertTrue(homePage.validateSortFieldIsDisplayed(),"Expected Sort Field to be displayed.");
//        sleep(5000);
//    }

//    @Test
//    public void verify_search_button_is_displayed() {
//        assertTrue(homePage.validateThatSearchButtonIsDisplayed(), "Expected search button to be displayed and enabled.");
//        sleep(5000);
//    }

    @Test
    public void verify_header_greetings_message(){
        assertEquals(homePage.getHeader().validateGreetingsMessageIsDisplayed(), "Hello guest!", "Expected greetings message to be: Hello guest!");
        sleep(3000);
    }

    @Test
    public void verify_header_contains_sign_in_icon(){
        assertTrue(homePage.getHeader().validateSignInIconIsDisplayed(), "Expected Sign In icon to be displayed.");
        sleep(3000);
    }

    @Test
    public void verify_header_contains_wishlist_icon(){
        assertTrue(homePage.getHeader().validateWishlistIconIsDisplayed(), "Expected Wishlist icon to be displayed.");
        sleep(3000);
    }

    @Test
    public void verify_header_contains_shopping_cart_icon(){
        assertTrue(homePage.getHeader().validateShoppingCartIconIsDisplayed(), "Expected Shopping Cart icon to be displayed.");
        sleep(3000);
    }

    @Test
    public void verify_header_contains_logo_icon() {
        assertTrue(homePage.getHeader().validateLogoIconIsDisplayed(), "Expected Logo icon to be displayed.");
        sleep(3000);
    }
}
